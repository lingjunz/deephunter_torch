# deephunter_torch

## 1. brief introduction

This reporitory is the pytorch version of deephunter(2.0) which is equipped with **ood_guided** strategy. 

## 2. guided engine

The guided engine support three modes:

* only neuron coverage guided mode
* only ood guided mode
* combination mode

## 3. example command

* For profiling a pytorch DNN model(refer to `ProfileNN.py`):

``` bash
python ProfileNN.py -net lenet5 -oe 1 -o ../DeepEvolve/net_profiling -data_type mnist
```

* For running with different modes:

```bash
python image_fuzzer_torch.py -i ./seeds/mnist_test_200_lenet5 -o ./mnist_output -model lenet5 -guided_mode 2 -criteria kmnc -comb_cri kmnc -ood_params 100 -max_iteration 200000
python image_fuzzer_torch.py -i ./seeds/mnist_test_200_lenet5 -o ./mnist_output_2 -model lenet5 -guided_mode 2 -ood_params 100 -criteria nc -comb_cri kmnc -metric_para 0.5 -max_iteration 1000
python image_fuzzer_torch.py -i ./seeds/mnist_test_200_lenet5 -o ./mnist_output_1 -model lenet5 -guided_mode 1 -ood_params 100 -criteria nc -comb_cri kmnc  -max_iteration 1000
```

* For continuous fuzzing, we prepare three scripts: `start_densenet(/lenet5/resnet18).sh`

## 4. datasets

* For initial seeds, you can find them in *seeds* folder.
* For output results we collected from lenet5,resnet18 and densenet, you can download them via this [link](https://drive.google.com/open?id=1HU6u_z_Dy-hoDIViBXbdT6Gnxu_ICbV-).
* For pretrained models, you can download them via this [link](https://drive.google.com/open?id=12vKYgtJcBkssshxnFVmNJSq9lxMLgMoC).

## 5. Citation

If you find this reporitory is helpful, please cite following paper(s).

* 	Xiaofei Xie, Lei Ma, Felix Juefei-Xu, Minhui Xue, Hongxu Chen, Yang Liu, Jianjun Zhao, Bo Li, Jianxiong Yin, Simon See: DeepHunter: a coverage-guided fuzz testing framework for deep neural networks. ISSTA 2019: 146-157