import numpy as np
import torch
import torch.nn.functional as F

concat = lambda x: np.concatenate(x,axis=0)
to_np = lambda x: x.data.cpu().numpy()


def cal_accuracy(net,test_loader,info=True):
    net.eval()
    _preds = []
    _top1_conf = []
    correct = 0
    total_loss = 0.0
    with torch.no_grad():
        for data,target in test_loader:
            target = target.long()
            data,target = data.cuda(),target.cuda()
            # forward
            output = net(data)
            smax = to_np(F.softmax(output,dim=1))
            loss = F.cross_entropy(output, target)
            # accuracy
            cur_preds = np.argmax(smax, axis=1)
            _preds.append(cur_preds)
            
            temp = np.sort(-1*smax)
            top1_conf = -1*(temp[:,0])#- temp[:,1])
            _top1_conf.append(top1_conf)
            targets = to_np(target)
            correct += np.sum(cur_preds == targets)
            # test loss
            total_loss += float(loss.data)
    
    test_loss = total_loss / len(test_loader)
    test_acc = correct / len(test_loader.dataset)
    if info:
        print(">>>Accuracy:%.5f,Loss:%.4f,%d/%d"%(test_acc,test_loss,correct,len(test_loader.dataset)))
    return test_acc, concat(_preds), concat(_top1_conf)

