import numpy as np
import torch
import torch.nn.functional as F
# ========================================
# Function: calc_MSE
# ========================================

concat = lambda x: np.concatenate(x,axis=0)
to_np = lambda x: x.data.cpu().numpy()

def get_dae_scores(net, loader,ood_max=1, ood_min=0, noise_factor=0.5): 
    
    _scores = []
    _image_input = []
    _noise_input = []
    _recons_output = []
    
    net.eval()
    divider = 1.0/(ood_max- ood_min)
    
    with torch.no_grad():
        for batch_idx,(images,_) in enumerate(loader):
            
            # add noise to the test images
            noisy_imgs = images + noise_factor * torch.randn(*images.shape)
            noisy_imgs = np.clip(noisy_imgs, 0., 1.)
            noisy_imgs = noisy_imgs.cuda()
            
            output = net(noisy_imgs)
            images = images.cuda()
            loss = F.mse_loss(images,output,reduce=0)
            if len(images.shape)==3:
                orig_scores = to_np(loss).mean((1,2))
            elif len(images.shape)==4:
                orig_scores = to_np(loss).mean((1,2,3))
            else:assert(False)
            
            new_oods = (orig_scores - ood_min) * divider
            if not (ood_max==1 and ood_min==0):
                new_oods[new_oods<0] = 0.0
                new_oods[new_oods>1] = 1.0
            _scores.append(new_oods)
            _image_input.append(to_np(images))
            _noise_input.append(to_np(noisy_imgs))
            _recons_output.append(to_np(output))
    
    _image_input,_noise_input,_recons_output = concat(_image_input), concat(_noise_input), concat(_recons_output)
    _image_input,_noise_input,_recons_output = np.uint8(_image_input*255).squeeze(), np.uint8(_noise_input*255).squeeze(), np.uint8(_recons_output*255).squeeze()
    
    return concat(_scores),_image_input,_noise_input,_recons_output

