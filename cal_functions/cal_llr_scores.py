
import numpy as np
from torch.autograd import Variable
import torch.nn.functional as F


concat = lambda x: np.concatenate(x,axis=0)
to_np = lambda x: x.data.cpu().numpy()


def compute_likelihood(dataloader, net):
    net.eval()
    likelihoods = []
    for input,_ in dataloader:
        input = input.cuda()
        output = net(input)
        target = Variable((input.data[:,0]).long()).cuda()
        loss = F.nll_loss(F.log_softmax(output,dim=1), target,reduction='none')
        cur = -1 * np.sum(to_np(loss),axis=(1,2))
        likelihoods .append(cur)
    return concat(likelihoods)


def compute_likelihood_ratio(dataloader, net, back_net):
    all_likelihood = compute_likelihood(dataloader, net)
    back_likelihood = compute_likelihood(dataloader, back_net)
    return all_likelihood - back_likelihood