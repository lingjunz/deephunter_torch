import os
import numpy as np
from torch.autograd import Variable
import torch.nn.functional as F
from detector.mahalanobis import lib_generation
import torch


concat = lambda x: np.concatenate(x,axis=0)
to_np = lambda x: x.data.cpu().numpy()



def get_mahalanobis_scores(net, dataloader,
                      magnitude= 0.0001, 
                      model_name = "densenet", 
                      num_classes = 10,
                      data_type="cifar"):
    
    net.eval()
    if data_type=="cifar":
        temp_x = torch.rand(2,3,32,32).cuda()
    else:
        temp_x = torch.rand(2,1,28,28).cuda()
    temp_x = Variable(temp_x)
    temp_list = net.feature_list(temp_x)[1]
    num_output = len(temp_list)
#     feature_list = np.empty(num_output)
#     count = 0
#     for out in temp_list:
#         feature_list[count] = out.size(1)
#         count += 1
    print('get sample mean and covariance')
    sample_mean = np.load("/home/lingjun/ood_2020/ood_attack/detector/mahalanobis/mean_precision/{}_mean({}).npy".format(data_type,model_name),allow_pickle=True).tolist()
    precision = np.load("/home/lingjun/ood_2020/ood_attack/detector/mahalanobis/mean_precision/{}_precision({}).npy".format(data_type,model_name),allow_pickle=True).tolist()
    
    print('Noise: ' + str(magnitude))
    
    Mahalanobis_score = lib_generation.get_raw_mahalanobis(net,num_output, dataloader, sample_mean, precision, magnitude, data_type, num_classes)
    
    
    return Mahalanobis_score
    


