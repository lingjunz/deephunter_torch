import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd.gradcheck import zero_gradients

from torch.autograd import Variable

concat = lambda x: np.concatenate(x,axis=0)
to_np = lambda x: x.data.cpu().numpy()

# densenet cifar: magnitude = 0.0005, temper = 10
# resnet: {'TNR': 0.31420000000000003, 'AUROC': 0.74278394, 'AUPR': 0.72931522335714} {'magnitude': 0.005, 'temper': 10}
# wrn: magnitude = 0.002, temper = 10, {'TNR': 0.7962, 'AUROC': 0.95731588, 'AUPR': 0.9563721272728706}
# VGG13: {'TNR': 0.7798, 'AUROC': 0.9514171199999999, 'AUPR': 0.9508505874738289}  {'magnitude': 0.0015, 'temper': 4}
def get_odin_scores(net, loader, data_type = "cifar" ,magnitude = 0.0005, temper = 10,ood_max=1, ood_min=0,in_dist = False, use_xent = False): 
    _scores = []
    _preds = []
    _right_score = []
    _wrong_score = []
    _top2_diff = []
    net.eval()
    divider = 1.0/(ood_max- ood_min)
    # here we need to compute the sign of gradient of cross entropy loss w.r.t. input
    for batch_idx,(img,target) in enumerate(loader):
        
        img = img.cuda()
        img_var = Variable(img,requires_grad=True)
        zero_gradients(img_var)
        output = net(img_var)/temper
        orig_preds = torch.argmax(output,dim=1)
        orig_xent =nn.CrossEntropyLoss()(output,orig_preds) #  F.cross_entropy
        orig_xent.backward()
        
#         gradient =  torch.sign(img_var.grad.data)
        gradient =  torch.ge(img_var.grad.data, 0)
        gradient = (gradient.float() - 0.5) * 2
        if data_type == "cifar":
            gradient.index_copy_(1, torch.LongTensor([0]).cuda(), gradient.index_select(1, torch.LongTensor([0]).cuda()) / (63.0/255.0))
            gradient.index_copy_(1, torch.LongTensor([1]).cuda(), gradient.index_select(1, torch.LongTensor([1]).cuda()) / (62.1/255.0))
            gradient.index_copy_(1, torch.LongTensor([2]).cuda(), gradient.index_select(1, torch.LongTensor([2]).cuda()) / (66.7/255.0))
        
        tempInputs = torch.add(img_var.data, -magnitude, gradient)
        
#         perturbation = -1 * epsilon * torch.sign(img_var.grad.data)
        
#         temp_img = img_var.data + perturbation
#         img_var.data = temp_img
        with torch.no_grad():
            output = net(Variable(tempInputs))/temper


            smax = to_np(F.softmax(output,dim=1))

            cur_preds = np.argmax(smax, axis=1)
            _preds.append(cur_preds)

            temp = np.sort(-1*smax)
            top2_diff = -1*(temp[:,0])#- temp[:,1])
            _top2_diff.append(top2_diff)

            if use_xent:
                orig_scores = to_np((output.mean(1) - torch.logsumexp(output,dim=1)))
            else:
                orig_scores = 1-np.max(smax,axis=1)

            new_oods = (orig_scores - ood_min) * divider
            if not (ood_max==1 and ood_min==0):
                new_oods[new_oods<0] = 0.0
                new_oods[new_oods>1] = 1.0
            _scores.append(new_oods)

            if in_dist:
                targets = target.numpy().squeeze()
                right_indices = cur_preds == targets
                wrong_indices = np.invert(right_indices)

                if use_xent:
                    _right_score.append(new_oods[right_indices])
                    _wrong_score.append(new_oods[wrong_indices])
                else:
                    _right_score.append(1-np.max(smax[right_indices], axis=1))
                    _wrong_score.append(1-np.max(smax[wrong_indices], axis=1))
            
    if in_dist:
        return concat(_top2_diff),concat(_scores), concat(_right_score), concat(_wrong_score),concat(_preds)
    else:
        return concat(_top2_diff),concat(_scores),concat(_preds)


