import numpy as np
import torch
import torch.nn.functional as F

concat = lambda x: np.concatenate(x,axis=0)
to_np = lambda x: x.data.cpu().numpy()

def get_oe_scores(net, loader,ood_max=1, ood_min=0,use_xent = False,in_dist = False): 
    _scores = []
    _preds = []
    _right_score = []
    _wrong_score = []
    _top2_diff = []
    net.eval()
    divider = 1.0/(ood_max- ood_min)
    with torch.no_grad():
        for batch_idx,(data,target) in enumerate(loader):
            data = data.cuda()
            output = net(data)
            smax = to_np(F.softmax(output,dim=1))
            
            cur_preds = np.argmax(smax, axis=1)
            _preds.append(cur_preds)
            
            temp = np.sort(-1*smax)
            top2_diff = -1*(temp[:,0])#- temp[:,1])
            _top2_diff.append(top2_diff)
            
            if use_xent:
                orig_scores = to_np((output.mean(1) - torch.logsumexp(output,dim=1)))
            else:
                orig_scores = 1-np.max(smax,axis=1)
                
            new_oods = (orig_scores - ood_min) * divider
            if not (ood_max==1 and ood_min==0):
                new_oods[new_oods<0] = 0.0
                new_oods[new_oods>1] = 1.0
            _scores.append(new_oods)
            
            if in_dist:
                targets = target.numpy().squeeze()
                right_indices = cur_preds == targets
                wrong_indices = np.invert(right_indices)

                if use_xent:
                    _right_score.append(new_oods[right_indices])
                    _wrong_score.append(new_oods[wrong_indices])
                else:
                    _right_score.append(1-np.max(smax[right_indices], axis=1))
                    _wrong_score.append(1-np.max(smax[wrong_indices], axis=1))
            
        if in_dist:
            return concat(_top2_diff),concat(_scores), concat(_right_score), concat(_wrong_score),concat(_preds)
        else:
            return concat(_top2_diff),concat(_scores),concat(_preds)


