
import numpy as np
import sys
from collections import OrderedDict,defaultdict
import torch
concat = lambda x: np.concatenate(x,axis=0)
to_np = lambda x: x.data.cpu().numpy()

class Coverage():
    def __init__(self, net, guided_mode,criteria, comb_cri,nc_sections,ood_sections, total_neurons,metric_para,
                                                class_num=10, profiling_dict={}, name_list = [],layer_start_index=[]):
        
        self.net = net
        self.guided_mode = guided_mode
        self.criteria = criteria
        self.comb_cri = comb_cri
        self.nc_sections = nc_sections
        self.ood_sections = ood_sections
        self.total_neurons = total_neurons
        self.class_num = class_num
        self.profiling_dict = profiling_dict
        self.name_list = name_list
        self.layer_start_index = layer_start_index
        self.metric_para = metric_para
        self.nc_sections_dic = {"nc":1,"nbc":(nc_sections+1) * 2,"snac":nc_sections+1,'nc_threshold':metric_para}

        if guided_mode==0:
            self.total_size = total_neurons * self.nc_sections
        elif guided_mode==1:
            self.total_size = class_num * ood_sections
        else :
            self.total_size = class_num * ood_sections + total_neurons * self.nc_sections
        
    def feature_list(self,input_data):
        _, out_list, _ = self.net.feature_list(input_data)
        return out_list

    def kmnc_update_coverage(self, feature_list, name_list,  coverage_map, skip_ood=True):

        for idx, layer_name in enumerate(name_list):
            if layer_name=="ood" and skip_ood:
                continue
            
            layer_outputs = to_np(feature_list[idx]) if isinstance(feature_list[idx] ,torch.Tensor) else feature_list[idx]
            for seed_id, layer_output in enumerate(layer_outputs):
                cur_neurons = layer_output.shape[0] 
                for neuron_idx in range(cur_neurons):
                    if layer_name == "ood":
                        assert cur_neurons == 2, layer_output.shape
                        neuron_idx = int(layer_output[-1]) # layer_output-> [oe_scores,preds]
                        output = layer_output[0]
                        lower_bound = 0.0
                        upper_bound = 1.0
                        unit_range = (upper_bound - lower_bound) / self.ood_sections
                        layer_start_index = self.layer_start_index[-1]
                    else:
#                         print(layer_output[neuron_idx,...].shape)
                        output =np.mean(layer_output[neuron_idx,...])      
    
                        profiling_data_list = self.profiling_dict[(layer_name, neuron_idx)]
                        mean_value = profiling_data_list[0]
                        std = profiling_data_list[2]
                        lower_bound = profiling_data_list[3]
                        upper_bound = profiling_data_list[4]
                        unit_range = (upper_bound - lower_bound) / self.nc_sections
                    # the special case, that a neuron output profiling is a fixed value
                    # TODO: current solution see whether test data cover the specific value
                    # if it covers the value, then it covers the entire range by setting to all 1s
                    if unit_range == 0:
                        continue
                    # we ignore output cases, where output goes out of profiled ranges,
                    # this could be the surprised/exceptional case, and we leave it to
                    # neuron boundary coverage criteria
                    if output > upper_bound or output < lower_bound:
#                         print(output,"out of bound({},{}),label:{}".format(lower_bound,upper_bound,neuron_idx))
                        continue
                    subrange_index = int((output - lower_bound) / unit_range)
                    if subrange_index == self.nc_sections or subrange_index == self.ood_sections:
                        subrange_index -= 1
                    if layer_name != "ood":
                        id = int(self.layer_start_index[idx] + neuron_idx) * self.nc_sections + subrange_index
                    else:
                        id = int(layer_start_index * self.nc_sections + neuron_idx * self.ood_sections + subrange_index)

                    if not coverage_map[seed_id][id]:
                        coverage_map[seed_id][id] = 1

    def scale(self, layer_outputs, rmax=1, rmin=0):
        '''
        scale the intermediate layer's output between 0 and 1
        :param layer_outputs: the layer's output tensor
        :param rmax: the upper bound of scale
        :param rmin: the lower bound of scale
        :return:
        '''
        divider = (layer_outputs.max() - layer_outputs.min())
        if divider == 0:
            return np.zeros(shape=layer_outputs.shape)
        X_std = (layer_outputs - layer_outputs.min()) / divider
        X_scaled = X_std * (rmax - rmin) + rmin
        return X_scaled

    def nc_update_coverage(self,  feature_list, name_list, coverage_map, skip_ood=True):
        '''
                Given the input, update the neuron covered in the model by this input.
                    This includes mark the neurons covered by this input as "covered"
                :param input_data: the input image
                :return: the neurons that can be covered by the input
                '''
        for idx, layer_name in enumerate(name_list):
            if layer_name=="ood" and skip_ood:
                continue
#             layer_outputs = to_np(feature_list[idx]) if layer_name != "ood" else feature_list[idx]
            layer_outputs = to_np(feature_list[idx]) if isinstance(feature_list[idx] ,torch.Tensor) else feature_list[idx]
            for seed_id, layer_output in enumerate(layer_outputs):
#                 scaled = self.scale(layer_output)
                cur_neurons = layer_output.shape[0] 
                for neuron_idx in range(cur_neurons):
                    output =np.mean(layer_output[neuron_idx,...]) 
                    if output > 0:#self.nc_sections_dic["nc_threshold"]:
                        
                        id =  (self.layer_start_index[idx] + neuron_idx) * self.nc_sections + 0
                        coverage_map[seed_id][id] = 1

    def bknc_update_coverage(self, feature_list, name_list, coverage_map,  rev, skip_ood=True):
        for idx, layer_name in enumerate(name_list):
            if layer_name=="ood" and skip_ood:
                continue
            layer_outputs = to_np(feature_list[idx]) if isinstance(feature_list[idx] ,torch.Tensor) else feature_list[idx]
            for seed_id, layer_output in enumerate(layer_outputs):
                layer_output_dict = defaultdict()
                cur_neurons = layer_output.shape[0] 
                for neuron_idx in range(cur_neurons):
                    output =np.mean(layer_output[neuron_idx,...])      
                    layer_output_dict[neuron_idx] = output

                # sort the dict entry order by values
                sorted_index_output_dict = OrderedDict(
                    sorted(layer_output_dict.items(), key=lambda x: x[1], reverse=rev))
                # for list if the top_k > current layer neuron number,
                # the whole list would be used, not out of bound
                top_k_node_index_list = list(sorted_index_output_dict.keys())[:self.nc_sections]
                for top_sec, top_idx in enumerate(top_k_node_index_list):
                    id = int(self.layer_start_index[idx] + top_idx) * self.nc_sections + top_sec
                    if not coverage_map[seed_id][id]:
                        coverage_map[seed_id][id] = 1

    def nbc_update_coverage(self ,feature_list, name_list, coverage_map,skip_ood=True ):
        for idx, layer_name in enumerate(name_list):
            if layer_name=="ood" and skip_ood:
                continue
            layer_outputs = to_np(feature_list[idx]) if isinstance(feature_list[idx] ,torch.Tensor) else feature_list[idx]
            for seed_id, layer_output in enumerate(layer_outputs):
                cur_neurons = layer_output.shape[0] 
                for neuron_idx in range(cur_neurons):
                    output =np.mean(layer_output[neuron_idx,...])   

                    profiling_data_list = self.profiling_dict[(layer_name, neuron_idx)]
                    # mean_value = profiling_data_list[0]
                    # std = profiling_data_list[2]
                    lower_bound = profiling_data_list[3]
                    upper_bound = profiling_data_list[4]
                    # this version uses k multi_section as unit range, instead of sigma
                    # TODO: need to handle special case, std=0
                    # TODO: this might be moved to args later
                    k_multisection = 1000
                    unit_range = (upper_bound - lower_bound) / k_multisection
                    if unit_range == 0:
                        unit_range = 0.05

                    
                    if output < lower_bound: # the hypo active case, the store targets from low to -infi
                        # float here
                        target_idx = (lower_bound - output) / unit_range
                    elif output > upper_bound:  # the hyperactive case
                        target_idx = (output - upper_bound) / unit_range
                    else:
                        continue

                    if target_idx > (self.nc_sections - 1):
                        id = (self.layer_start_index[idx] + neuron_idx )* self.nc_sections + self.nc_sections - 1
                    else:
                        id = (self.layer_start_index[idx] + neuron_idx ) * self.nc_sections + int(target_idx)
                    id = int(id)
                    if not coverage_map[seed_id][id]:
                        coverage_map[seed_id][id] = 1
    
    def snac_update_coverage(self, feature_list, name_list, coverage_map ,skip_ood=True):
        for idx, layer_name in enumerate(name_list):
            if layer_name=="ood" and skip_ood:
                continue
            layer_outputs = to_np(feature_list[idx]) if isinstance(feature_list[idx] ,torch.Tensor) else feature_list[idx]
            for seed_id, layer_output in enumerate(layer_outputs):
                cur_neurons = layer_output.shape[0] 
                for neuron_idx in range(cur_neurons):
                    output =np.mean(layer_output[neuron_idx,...])   

                    profiling_data_list = self.profiling_dict[(layer_name, neuron_idx)]
                    # mean_value = profiling_data_list[0]
                    # std = profiling_data_list[2]
                    lower_bound = profiling_data_list[3]
                    upper_bound = profiling_data_list[4]

                    # this version uses k multi_section as unit range, instead of sigma
                    # TODO: need to handle special case, std=0
                    # TODO: this might be moved to args later
                    # this supposes that the unit range of boundary range is the same as k multi-1000
                    k_multisection = 1000
                    unit_range = (upper_bound - lower_bound) / k_multisection
                    if unit_range == 0:
                        unit_range = 0.05
                    
                    # the hyperactive case
                    if output > upper_bound:
                        target_idx = (output - upper_bound) / unit_range

                        if target_idx > (self.nc_sections - 1):
                            id = (self.layer_start_index[idx] + neuron_idx )* self.nc_sections + self.nc_sections - 1
                        else:
                            id = (self.layer_start_index[idx] + neuron_idx ) * self.nc_sections + int(target_idx)
                        id = int(id)
                        if not coverage_map[seed_id][id]:
                            coverage_map[seed_id][id] = 1

    def update_coverage(self, feature_list):
        batch_num = len(feature_list[0])
        coverage_map = np.tile(np.zeros(self.total_size, dtype=np.uint8), (batch_num,1))
        if self.guided_mode==0: # only nc mode
            self.__only_nc_update(feature_list,coverage_map)
        elif self.guided_mode==1:  # only ood mode
            if self.comb_cri == 'kmnc':
                self.kmnc_update_coverage(feature_list,self.name_list, coverage_map,skip_ood=False)
        elif self.guided_mode==2:
            self.__comb_mode_update(feature_list,coverage_map)
        else:
            assert False,"undefined guided mode: {}".format(self.guided_mode)
        return coverage_map

    def __only_nc_update(self, feature_list,coverage_map):
        if self.criteria == 'kmnc':
            self.kmnc_update_coverage(feature_list,self.name_list, coverage_map)
        elif self.criteria == 'bknc':
            self.bknc_update_coverage(feature_list,self.name_list, coverage_map,False)
        elif self.criteria == 'tknc':
            self.bknc_update_coverage(feature_list,self.name_list, coverage_map,True)
        elif self.criteria == 'nbc':
            self.nbc_update_coverage(feature_list,self.name_list,coverage_map)
        elif self.criteria == 'snac':
            self.snac_update_coverage(feature_list,self.name_list,coverage_map)
        elif self.criteria == 'nc':
            self.nc_update_coverage(feature_list,self.name_list,coverage_map)
        else:
            print("* please select the correct coverage criteria as feedback:")
            print("['nc', 'kmnc', 'nbc', 'snac', 'bknc', 'tknc' ]")
            assert False
    
    def __comb_mode_update(self, feature_list,coverage_map):
        assert self.comb_cri == "kmnc","only support kmnc for ood guided"
        if self.criteria == 'kmnc'  :
            self.kmnc_update_coverage(feature_list, self.name_list, coverage_map,skip_ood=False)
        elif self.criteria == 'nc' :
            self.nc_update_coverage(feature_list, self.name_list ,coverage_map)
            self.kmnc_update_coverage(feature_list[-1:], self.name_list[-1:] ,coverage_map,skip_ood=False)
        elif self.criteria == 'bknc':
            self.bknc_update_coverage(feature_list,self.name_list, coverage_map,False)
            self.kmnc_update_coverage(feature_list[-1:], self.name_list[-1:] ,coverage_map,skip_ood=False)
        elif self.criteria == 'tknc':
            self.bknc_update_coverage(feature_list,self.name_list, coverage_map,True)
            self.kmnc_update_coverage(feature_list[-1:], self.name_list[-1:] ,coverage_map,skip_ood=False)
        elif self.criteria == 'nbc':
            self.nbc_update_coverage(feature_list,self.name_list,coverage_map)
            self.kmnc_update_coverage(feature_list[-1:], self.name_list[-1:] ,coverage_map,skip_ood=False)
        elif self.criteria == 'snac':
            self.snac_update_coverage(feature_list,self.name_list,coverage_map)
            self.kmnc_update_coverage(feature_list[-1:], self.name_list[-1:] ,coverage_map,skip_ood=False)
        else:
            assert False,"undefined option,criteria:{},comb_cri:{}".format(self.criteria,self.comb_cri)
