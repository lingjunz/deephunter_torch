
import tensorflow as tf
import numpy as np
import time
import os
from random import  randint
import datetime
import random

class FuzzQueue(object):
    """Class that holds inputs and associated coverage."""

    def __init__(self,  outdir, is_random, sample_type, cov_num, guided_mode,criteria,comb_cri,class_num,ood_sections):
        """Init the class.
        """
        self.plot_file = open(os.path.join(outdir, 'plot.log'), 'a+')
        self.out_dir = outdir
        self.mutations_processed = 0
        self.queue = []
        self.sample_type = sample_type
        self.start_time = time.time()
        # whether it is random testing
        self.random = is_random
        self.guided_mode = guided_mode
        self.criteria = criteria
        self.comb_cri = comb_cri
        self.class_num = class_num
        self.ood_sections = ood_sections
        self.total_ood_sections = self.class_num * self.ood_sections 
        
        self.log_time = time.time()
        # Like AFL, it records the coverage of the seeds in the queue
        self.coverage_map = np.zeros(cov_num,dtype=np.uint8)

        self.uniq_crashes = 0
        self.total_queue = 0
        self.total_cov = cov_num

        # Some log information
        self.last_crash_time = self.start_time
        self.last_reg_time = self.start_time
        self.cur_pos = 0
        self.seed_attacked = set()
        self.seed_attacked_first_time = dict()
        

        self.dry_run_cov = None

        # REG_MIN and REG_GAMMA are the p_min and gamma in Equation 3
        self.REG_GAMMA = 5
        self.REG_MIN = 0.3
        self.REG_INIT_PROB = 0.8
    
    
    def has_new_bits(self, seed):
#         print(self.coverage_map.shape, seed.coverage)
        uncovered = 1 - self.coverage_map
        update_map = uncovered * seed.coverage
        has_new = np.sum(update_map)>0
        if has_new:
            # If the coverage is increased, we will update the coverage
            self.coverage_map = update_map + self.coverage_map
        return has_new or self.random
    
    def compute_cov(self):
        # Compute the current coverage in the queue
        coverage = round(float(np.sum(self.coverage_map)) * 100 / self.total_cov, 2)
        
        
        if self.ood_sections <= 0:
            ood_coverage = ""
        else:
            idx = -1*self.total_ood_sections
            ood_coverage = round(float(np.sum(self.coverage_map[idx:])) * 100 / self.total_ood_sections, 2)
        
        return str(coverage),str(ood_coverage)
        
        
    def select_next(self):
        # Different seed selection strategies (See details in Section 4)
        if self.random == 1 or self.sample_type == 'uniform':
            return self.random_select()
        elif self.sample_type == 'tensorfuzz':
            return self.tensorfuzz()
        elif self.sample_type == 'deeptest':
            return self.deeptest_next()
        elif self.sample_type == 'prob':
            return self.prob_next()
    
    def random_select(self):
        return random.choice(self.queue)

    def deeptest_next(self):
        choice = self.queue[-1]
        return choice
     
    def prob_next(self):
        """Grabs new input from corpus according to sample_function."""
        while True:
            if self.cur_pos == len(self.queue):
                self.cur_pos = 0

            cur_seed = self.queue[self.cur_pos]
            if randint(0,100) < cur_seed.probability * 100:
                # Based on the probability, we decide whether to select the current seed.
                cur_seed.fuzzed_time += 1
                self.cur_pos += 1
                return cur_seed
            else:
                self.cur_pos += 1

    def tensorfuzz(self):
        """Grabs new input from corpus according to sample_function."""
        # choice = self.sample_function(self)
        corpus = self.queue
        reservoir = corpus[-5:] + [random.choice(corpus)]
        choice = random.choice(reservoir)
        return choice
        # return random.choice(self.queue) 
    
    
    def fuzzer_handler(self, iteration, cur_seed, bug_found, coverage_inc):
        # The handler after each iteration
        if self.sample_type == 'deeptest' and not coverage_inc:
            # If deeptest cannot increase the coverage, it will pop the last seed from the queue
            self.queue.pop()

        elif self.sample_type == 'prob':
            # Update the probability based on the Equation 3 in the paper
            if cur_seed.probability > self.REG_MIN and cur_seed.fuzzed_time < self.REG_GAMMA * (1 - self.REG_MIN):
                cur_seed.probability = self.REG_INIT_PROB - float(cur_seed.fuzzed_time) / self.REG_GAMMA

        if bug_found:
            # Log the initial seed from which we found the adversarial. It is for the statics of Table 6
            self.seed_attacked.add(cur_seed.root_seed)
            if not (cur_seed.parent in self.seed_attacked_first_time):
                # Log the information about when (which iteration) the initial seed is attacked successfully.
                self.seed_attacked_first_time[cur_seed.root_seed] = iteration

    
    
    def plot_log(self, id):
        # Plot the data during fuzzing, include: the current time, current iteration, length of queue, initial coverage,
        # total coverage, number of crashes, number of seeds that are attacked, number of mutations, mutation speed
        queue_len = len(self.queue)
        coverage,ood_coverage = self.compute_cov()
        current_time = time.time()
        self.plot_file.write(
            "%d,%d,%d,%s,%s,%s,%d,%d,%s,%s\n" %
            (time.time(),
             id,
             queue_len,
             self.dry_run_cov,
             coverage,ood_coverage,
             self.uniq_crashes,
             len(self.seed_attacked),
             self.mutations_processed,
             round(float(self.mutations_processed) / (current_time - self.start_time), 2)
             ))
        self.plot_file.flush()
        

    def log(self):
        queue_len = len(self.queue)
        coverage,ood_coverage = self.compute_cov()
        current_time = time.time()
        print(
                "Metrics %d,  %s , %s| corpus_size %s | crashes_size %s | mutations_per_second: %s | total_exces %s | last new reg: %s | last new adv %s | coverage: %s -> %s(%s)%%"%(
                self.guided_mode,
                self.criteria,self.comb_cri,
                queue_len,
                self.uniq_crashes,
                round(float(self.mutations_processed)/(current_time - self.start_time), 2),
                self.mutations_processed,
                datetime.timedelta(seconds=(time.time() - self.last_reg_time)),
                datetime.timedelta(seconds=(time.time() - self.last_crash_time)),
                self.dry_run_cov,
                coverage,ood_coverage)
            )
        
     