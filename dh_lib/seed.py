



class Seed(object):
    """Class representing a single element of a corpus."""

    def __init__(self, mutation_type, coverage , root_seed, parent,ground_truth, pred_label, ood_score=-1, l0_ref=0, linf_ref=0):
        """Inits the object.

        Args:
          mutation_type: a transformation state to represent whether this seed has been
          coverage: a list to show the coverage
          root_seed: maintain the initial seed from which the current seed is sequentially mutated
          metadata: the prediction result
          ground_truth: the ground truth of the current seed

          l0_ref, linf_ref: if the current seed is mutated from affine transformation, we will record the l0, l_inf
          between initial image and the reference image. i.e., L0(s_0,s_{j-1}) L_inf(s_0,s_{j-1})  in Equation 2 of the paper
        Returns:
          Initialized object.
        """

        self.mutation_type =  mutation_type
        self.coverage = coverage
        
        self.root_seed = root_seed
        self.parent = parent
        self.pred_label = pred_label
        self.ground_truth = ground_truth
        self.ood_score = ood_score
        self.l0_ref = l0_ref
        self.linf_ref = linf_ref
        
        self.queue_time = None
        self.id = None
        # The initial probability to select the current seed.
        self.probability = 0.8
        self.fuzzed_time = 0

        
        
        
        
        
        