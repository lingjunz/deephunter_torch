import os
import sys
sys.path.append("..")
import torch
import numpy as np

from utils.configs import *
from handler import metadata_function
from cal_functions.cal_oe_scores import get_oe_scores
from utils.helper_function import make_dataloader
from dh_lib.seed import Seed
concat = lambda x: np.concatenate(x,axis=0)
to_np = lambda x: x.data.cpu().numpy()



def build_fetch_function(handler, preprocess, data_type, device, models=None):
    def func(input_batches):
        """The fetch function."""
        if models is not None:
            return quantize_fetch_function(
                handler,
                input_batches,
                preprocess,
                models
            )
        else:
            return fetch_function(
                handler,
                input_batches,
                preprocess,
                data_type,
                device
            )
    return func

def fetch_function(coverage_handler, input_batches, preprocess, data_type,device):
    _, img_batches, _, _, _ = input_batches
    if len(img_batches) == 0:
        return None, None
    
    preprocessed = preprocess(img_batches)
    preprocessed_new = torch.Tensor(preprocessed).to(device)
    layer_outputs = coverage_handler.feature_list(preprocessed_new)
    preds = np.argmax(to_np(layer_outputs[-1]), axis=1)
    preds = np.expand_dims(preds, axis=-1)

    if coverage_handler.guided_mode == 0:
        pass
    elif coverage_handler.guided_mode == 1:
        if data_type == "mnist":
            test_transform = transforms_dic['mnist']
            ood_loader = make_dataloader(img_batches,labels = None,img_size=(28,28), batch_size=256, transform_test=test_transform)
        elif data_type == "cifar10":
#             print(img_batches.shape)
            test_transform = transforms_dic['cifar10_test']
            ood_loader = make_dataloader(img_batches,labels = None,img_size=(32,32), batch_size=256, transform_test=test_transform)
        _,ood_scores,_ = get_oe_scores(coverage_handler.net,ood_loader,use_xent=False)
        ood_scores = np.expand_dims(ood_scores, axis=-1)
        layer_outputs = [np.concatenate((ood_scores,preds),axis=1)]
    
    elif coverage_handler.guided_mode == 2:
        if data_type == "mnist":
            test_transform = transforms_dic['mnist']
            ood_loader = make_dataloader(img_batches,labels=None,img_size=(28,28), batch_size=256, transform_test=test_transform)
        elif data_type == "cifar10":
#             print(img_batches.shape,img_batches.transpose(0,3,1,2))
            test_transform = transforms_dic['cifar10_test']
            ood_loader = make_dataloader(img_batches,labels=None,img_size=(32,32), batch_size=256, transform_test=test_transform)
        _,ood_scores,_ = get_oe_scores(coverage_handler.net,ood_loader,use_xent=False)
        ood_scores = np.expand_dims(ood_scores, axis=-1)
        layer_outputs = layer_outputs + [np.concatenate((ood_scores,preds),axis=1)]
    else:
        assert False,"undefined guided_mode : {}".format(coverage_handler.guided_mode)

    return layer_outputs, preds


def dry_run(indir, fetch_function,coverage_function, queue):

    seed_lis = np.sort(np.array([item for item in os.listdir(indir) if item.split(".")[-1]=="npy"]))
    # Read each initial seed and analyze the coverage
    for seed_name in seed_lis:
        print("Attempting dry run with '%s'..."%(seed_name))
        label = int(seed_name.split("_")[0])
        path = os.path.join(indir, seed_name)
        img = np.load(path)
        # Each seed will contain two images, i.e., the reference image and mutant (see the paper)
        input_batches = np.expand_dims(img[-1],axis=0)
        input_labels = np.array([label])
        # Predict the mutant and obtain the outputs
        # coverage_batches is the output of internal layers and metadata_batches is the output of the prediction result
        coverage_batches, metadata_batches = fetch_function((0, input_batches, 0, 0, 0))

        # Based on the output, compute the coverage information
        coverage_list = coverage_function(coverage_batches)
        metadata_list = metadata_function(metadata_batches)
        # Create a new seed
        input = Seed(mutation_type = 0, coverage = coverage_list[0],root_seed = seed_name,
                    parent = None, ground_truth = label, pred_label = metadata_list[0][0])
        
        
        new_img = np.append(input_batches, input_batches, axis=0)
        # Put the seed in the queue and save the npy file in the queue dir
        queue.save_if_interesting(seed = input, data = new_img, crash = False, dry_run = True, suffix = seed_name)


