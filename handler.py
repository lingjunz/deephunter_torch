import os
import sys
sys.path.append("..")
import numpy as np
from dh_lib.seed import Seed
from utils.helper_function import make_dataloader,transforms_dic
from cal_functions.cal_oe_scores import get_oe_scores


def iterate_function(names ):
    def func(queue, root_seed, parent, mutated_coverage_list, mutated_data_batches, mutated_metadata_list,
                         objective_function):

        ref_batches, batches, cl_batches, l0_batches, linf_batches = mutated_data_batches
        successed = False
        bug_found = False
        # For each mutant in the batch, we will check the coverage and whether it is a failed test
        for idx in range(len(mutated_coverage_list)):
            
            input = Seed(mutation_type = cl_batches[idx], coverage = mutated_coverage_list[idx], root_seed = root_seed,
                         parent = parent, ground_truth = parent.ground_truth,
                         pred_label = mutated_metadata_list[idx][0], 
                         l0_ref = l0_batches[idx], linf_ref = linf_batches[idx])
            # The implementation for the isFailedTest() in Algorithm 1 of the paper
            results = objective_function(input, names)

            if len(results) > 0:
                # We have find the failed test and save it in the crash dir.
                for i in results:
                    queue.save_if_interesting(input, batches[idx], True, suffix=i)
                bug_found = True
            else:
                new_img = np.append(ref_batches[idx:idx + 1], batches[idx:idx + 1], axis=0)
                # If it is not a failed test, we will check whether it has a coverage gain
                result = queue.save_if_interesting(input, new_img, False)
                successed = successed or result
        return bug_found, successed
    return func


def metadata_function(meta_batches):
    return meta_batches


def objective_function(seed, names):
    pred_label = seed.pred_label
    ground_truth = seed.ground_truth
    assert(names is not None)
    results = []
    if len(names)  == 1:
        # To check whether it is an adversarial sample
        if pred_label != ground_truth:
            results.append('')
    else:
        # To check whether it has different results between original model and quantized model
        # metadata[0] is the result of original model while metadata[1:] is the results of other models.
        # We use the string adv to represent different types;
        # adv = '' means the seed is not an adversarial sample in original model but has a different result in the
        # quantized version.  adv = 'a' means the seed is adversarial sample and has a different results in quan models.
        if pred_label == ground_truth:
            adv = ''
        else:
            adv = 'a'
        count = 1
        while count < len(metadata):
            if metadata[count] != ground_truth:
                results.append(names[count]+adv)
            count += 1

    # results records the suffix for the name of the failed tests
    return results
