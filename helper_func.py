
import numpy as np
from mutators import Mutators
import pickle
import torch
import os
import shutil

def check_dir(seed_output):
    # Build directory structure
    if os.path.exists(seed_output):
        shutil.rmtree(seed_output)
    img_dir =  os.path.join(seed_output, 'queue')
    crash_dir = os.path.join(seed_output, 'crashes')
    os.makedirs(crash_dir)
    os.makedirs(img_dir)
    plot_file = open(os.path.join(seed_output, 'plot.log'), 'a+')

    return  plot_file

def get_name_list(net,device,data_type):
    if data_type=="mnist":
        temp = np.random.randint(0,256,(2,1,28,28))
    else:
        temp = np.random.randint(0,256,(2,3,32,32))
    temp = torch.Tensor(temp/255.).to(device)
    _, feature_list, name_list = net.feature_list(temp)
    count = 0
    layer_start_index = []
    for i in range(len(name_list)):
        layer_start_index.append(count)
        count += feature_list[i].shape[1]
        print(name_list[i],"\t\t",feature_list[i].shape)
    print("Total neurons:{}".format(count))
    return name_list, layer_start_index, count


def load_pickle(filepath):
    profile_dict = pickle.load(open(filepath, 'rb'))
    print("Load {} successfully!".format(filepath))
    print("* profiling neuron size:", len(profile_dict.items()))
    return profile_dict

def dump(cov_dict, output_file):
    print("* profiling neuron size:", len(cov_dict.items()))
#     for item in cov_dict.items():
#         print(item)
    pickle_out = open(output_file, "wb")
    pickle.dump(cov_dict, pickle_out)
    pickle_out.close()

    print("write out profiling coverage results to ", output_file)
    print("done.")
    
def image_mutation_function(batch_num):
    # Given a seed, randomly generate a batch of mutants
    def func(seed):
        return Mutators.image_random_mutate(seed, batch_num)
    return func