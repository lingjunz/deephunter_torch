
import collections
import numpy as np
import torch
import pickle
import time
from keras.datasets import cifar10,mnist


from handler import metadata_function, iterate_function, objective_function
from helper_func import *
from fuzzone import build_fetch_function, dry_run
from coverage import Coverage

from utils.helper_function import load_net, make_dataloader
from utils.configs import *

from dh_lib.img_queue import ImageInputCorpus
from dh_lib.seed import Seed
from dh_lib.fuzzer import Fuzzer
import shutil
import argparse
# python image_fuzzer_torch.py -i ./seeds/mnist_test_200_lenet5 -o ./mnist_output -model lenet5 -guided_mode 2 -criteria kmnc -comb_cri kmnc -ood_params 100 -max_iteration 200000
# python image_fuzzer_torch.py -i ./seeds/mnist_test_200_lenet5 -o ./mnist_output_2 -model lenet5 -guided_mode 2 -ood_params 100 -criteria nc -comb_cri kmnc -metric_para 0.5 -max_iteration 1000
# python image_fuzzer_torch.py -i ./seeds/mnist_test_200_lenet5 -o ./mnist_output_1 -model lenet5 -guided_mode 1 -ood_params 100 -criteria nc -comb_cri kmnc  -max_iteration 1000



if __name__ == '__main__':

    device = torch.device('cuda:0')
    start_time = time.time()
   args = initial_argparse()

    class_num = args.class_num
    data_type = args.data_type
    seed_path = args.i
    outdir = args.o
    oe_mode = args.oe_mode
    model_name = args.model+"{}".format("_oe" if oe_mode==1 else "")
    guided_mode = args.guided_mode
    cri = args.criteria
    ood_sections = args.ood_params
    comb_cri = args.comb_cri
    metric_para = args.metric_para
    mutation_batch_num = args.batch_num
    max_iteration = args.max_iteration
    quantize_test = args.quantize_test
    quan_model_dir = args.quan_model_dir
    is_random = args.random
    select = args.select
    nc_sections = metrics_para[cri] if metric_para==-1 else metric_para
    nc_sections_dic = {"nc":1,"nbc":(nc_sections+1) * 2,"snac":nc_sections+1,'nc_threshold':metric_para}


    net = load_net(model_name,device = device)
    profile_dict = load_pickle(model_profile_path[model_name])
    preprocess = preprocess_dic[model_name]
    input_shape = shape_dic[model_name]
    name_list, layer_start_index,total_neurons = get_name_list(net,device,data_type)


    if guided_mode==0: # only nc mode
        if cri in nc_sections_dic.keys():
            nc_sections = nc_sections_dic[cri]
        cov_num = total_neurons * nc_sections
    elif guided_mode==1: # only ood mode
        layer_start_index = [0]
        name_list = ['ood']
        assert ood_sections != -1
        cov_num = class_num * ood_sections
    else : # nc + ood mode
        if cri in nc_sections_dic.keys():
            nc_sections = nc_sections_dic[cri]
        layer_start_index += [total_neurons]
        name_list += ['ood']
        cov_num = class_num * ood_sections + total_neurons * nc_sections


    model_names = [model_name]
    plot_file = check_dir(outdir)

    coverage_handler = Coverage(net = net, guided_mode=guided_mode,criteria = cri, comb_cri = comb_cri, 
                                nc_sections = nc_sections , ood_sections = ood_sections,total_neurons= total_neurons,
                                class_num = class_num,metric_para = metric_para,
                                profiling_dict = profile_dict, name_list = name_list,layer_start_index = layer_start_index)

    fetch_function = build_fetch_function(coverage_handler, preprocess,data_type,device)
    # The function to update coverage
    coverage_function = coverage_handler.update_coverage
    # The function to perform the mutation from one seed
    mutation_function = image_mutation_function(mutation_batch_num)
    # The seed queue
    queue = ImageInputCorpus(outdir, is_random, select, cov_num, guided_mode,cri,comb_cri,class_num,ood_sections)
    # Perform the dry_run process from the initial seeds
    dry_run(seed_path, fetch_function, coverage_function, queue)
    # For each seed, compute the coverage and check whether it is a "bug", i.e., adversarial example
    image_iterate_function = iterate_function(model_names)
     # The main fuzzer class
    fuzzer = Fuzzer(queue, coverage_function, metadata_function, objective_function, 
                    mutation_function, fetch_function, image_iterate_function, select)
    # The fuzzing process
    fuzzer.loop(max_iteration)
    print('finish', time.time() - start_time)