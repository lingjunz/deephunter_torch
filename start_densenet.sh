python image_fuzzer_torch.py -i ./seeds/cifar_test_200_densenet -o ./densenet_output/nc -model densenet -guided_mode 0  -criteria nc -metric_para 0.5 -max_iteration 2000 -data_type cifar10 
python image_fuzzer_torch.py -i ./seeds/cifar_test_200_densenet -o ./densenet_output/kmnc -model densenet -guided_mode 0  -criteria kmnc -max_iteration 2000 -data_type cifar10
python image_fuzzer_torch.py -i ./seeds/cifar_test_200_densenet -o ./densenet_output/nbc -model densenet -guided_mode 0  -criteria nbc  -max_iteration 2000 -data_type cifar10
python image_fuzzer_torch.py -i ./seeds/cifar_test_200_densenet -o ./densenet_output/snac -model densenet -guided_mode 0  -criteria snac -max_iteration 2000 -data_type cifar10
python image_fuzzer_torch.py -i ./seeds/cifar_test_200_densenet -o ./densenet_output/tknc -model densenet -guided_mode 0  -criteria tknc  -max_iteration 2000 -data_type cifar10


python image_fuzzer_torch.py -i ./seeds/cifar_test_200_densenet -o ./densenet_output/nc_ood -model densenet -guided_mode 2  -criteria nc -metric_para 0.5 -max_iteration 2000 -data_type cifar10 -comb_cri kmnc -ood_params 100
python image_fuzzer_torch.py -i ./seeds/cifar_test_200_densenet -o ./densenet_output/kmnc_ood -model densenet -guided_mode 2  -criteria kmnc -max_iteration 2000 -data_type cifar10 -comb_cri kmnc -ood_params 100
python image_fuzzer_torch.py -i ./seeds/cifar_test_200_densenet -o ./densenet_output/nbc_ood -model densenet -guided_mode 2  -criteria nbc  -max_iteration 2000 -data_type cifar10 -comb_cri kmnc -ood_params 100
python image_fuzzer_torch.py -i ./seeds/cifar_test_200_densenet -o ./densenet_output/snac_ood -model densenet -guided_mode 2  -criteria snac -max_iteration 2000 -data_type cifar10 -comb_cri kmnc -ood_params 100
python image_fuzzer_torch.py -i ./seeds/cifar_test_200_densenet -o ./densenet_output/tknc_ood -model densenet -guided_mode 2  -criteria tknc  -max_iteration 2000 -data_type cifar10 -comb_cri kmnc -ood_params 100