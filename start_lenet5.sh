python image_fuzzer_torch.py -i ./seeds/mnist_test_200_lenet5 -o ./mnist_output/nc -model lenet5 -guided_mode 0  -criteria nc -metric_para 0.5 -max_iteration 5000
python image_fuzzer_torch.py -i ./seeds/mnist_test_200_lenet5 -o ./mnist_output/kmnc -model lenet5 -guided_mode 0  -criteria kmnc -max_iteration 5000
python image_fuzzer_torch.py -i ./seeds/mnist_test_200_lenet5 -o ./mnist_output/nbc -model lenet5 -guided_mode 0  -criteria nbc  -max_iteration 5000
python image_fuzzer_torch.py -i ./seeds/mnist_test_200_lenet5 -o ./mnist_output/snac -model lenet5 -guided_mode 0  -criteria snac -max_iteration 5000
python image_fuzzer_torch.py -i ./seeds/mnist_test_200_lenet5 -o ./mnist_output/tknc -model lenet5 -guided_mode 0  -criteria tknc  -max_iteration 5000

# nbc tknc snac 

python image_fuzzer_torch.py -i ./seeds/mnist_test_200_lenet5 -o ./mnist_output/nc_ood -model lenet5 -guided_mode 2  -criteria nc -metric_para 0.5 -max_iteration 5000 -comb_cri kmnc -ood_params 100
python image_fuzzer_torch.py -i ./seeds/mnist_test_200_lenet5 -o ./mnist_output/kmnc_ood -model lenet5 -guided_mode 2  -criteria kmnc -max_iteration 5000 -comb_cri kmnc -ood_params 100
python image_fuzzer_torch.py -i ./seeds/mnist_test_200_lenet5 -o ./mnist_output/nbc_ood -model lenet5 -guided_mode 2  -criteria nbc  -max_iteration 5000 -comb_cri kmnc -ood_params 100
python image_fuzzer_torch.py -i ./seeds/mnist_test_200_lenet5 -o ./mnist_output/snac_ood -model lenet5 -guided_mode 2  -criteria snac -max_iteration 5000 -comb_cri kmnc -ood_params 100
python image_fuzzer_torch.py -i ./seeds/mnist_test_200_lenet5 -o ./mnist_output/tknc_ood -model lenet5 -guided_mode 2  -criteria tknc  -max_iteration 5000 -comb_cri kmnc -ood_params 100


