python image_fuzzer_torch.py -i ./seeds/cifar_test_200_resnet -o ./resnet18_output/nc -model resnet18 -guided_mode 0  -criteria nc -metric_para 0.5 -max_iteration 5000 -data_type cifar10 
python image_fuzzer_torch.py -i ./seeds/cifar_test_200_resnet -o ./resnet18_output/kmnc -model resnet18 -guided_mode 0  -criteria kmnc -max_iteration 5000 -data_type cifar10
python image_fuzzer_torch.py -i ./seeds/cifar_test_200_resnet -o ./resnet18_output/nbc -model resnet18 -guided_mode 0  -criteria nbc  -max_iteration 5000 -data_type cifar10
python image_fuzzer_torch.py -i ./seeds/cifar_test_200_resnet -o ./resnet18_output/snac -model resnet18 -guided_mode 0  -criteria snac -max_iteration 5000 -data_type cifar10
python image_fuzzer_torch.py -i ./seeds/cifar_test_200_resnet -o ./resnet18_output/tknc -model resnet18 -guided_mode 0  -criteria tknc  -max_iteration 5000 -data_type cifar10

python image_fuzzer_torch.py -i ./seeds/cifar_test_200_resnet -o ./resnet18_output/nc_ood -model resnet18 -guided_mode 2  -criteria nc -metric_para 0.5 -max_iteration 5000 -data_type cifar10 -comb_cri kmnc -ood_params 100
python image_fuzzer_torch.py -i ./seeds/cifar_test_200_resnet -o ./resnet18_output/kmnc_ood -model resnet18 -guided_mode 2  -criteria kmnc -max_iteration 5000 -data_type cifar10 -comb_cri kmnc -ood_params 100
python image_fuzzer_torch.py -i ./seeds/cifar_test_200_resnet -o ./resnet18_output/nbc_ood -model resnet18 -guided_mode 2  -criteria nbc  -max_iteration 5000 -data_type cifar10 -comb_cri kmnc -ood_params 100
python image_fuzzer_torch.py -i ./seeds/cifar_test_200_resnet -o ./resnet18_output/snac_ood -model resnet18 -guided_mode 2  -criteria snac -max_iteration 5000 -data_type cifar10 -comb_cri kmnc -ood_params 100
python image_fuzzer_torch.py -i ./seeds/cifar_test_200_resnet -o ./resnet18_output/tknc_ood -model resnet18 -guided_mode 2  -criteria tknc  -max_iteration 5000 -data_type cifar10 -comb_cri kmnc -ood_params 100
