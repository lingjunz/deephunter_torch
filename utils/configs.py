import numpy as np
import torchvision.transforms as trn
import sys
sys.path.append("..")

from models.ELOC_wideresnet import WideResNet
from models.vgg import VGG
from models.densenet import densenet_cifar #DenseNet3
from models.resnet import ResNet18
from models.lenet import Lenet5,Lenet4,Lenet1
from models.DAE import DenoisingAutoEncoder


# __all__ = ['attack_dic','transforms_dic','weight_path_dic','model_dic','data_dic']

mean = np.array([0.4914,0.4822,0.4465]) #np.array([x / 255 for x in [125.3, 123.0, 113.9]])
std = np.array([0.2470,0.2435,0.2616])#np.array([x / 255 for x in [63.0, 62.1, 66.7]])

# cifar100_mean = np.array([0.5071,0.4865,0.4409]) #50000 training samples
# cifar100_std = np.array([0.2673,0.2564,0.2762])

# svhn_mean = np.array([0.4377,0.4438,0.4728])
# svhn_std = np.array([0.1980,0.2010,0.1970])

# imagenet
# means = [0.485, 0.456, 0.406] 
# stds = [0.229, 0.224, 0.225]


cifar_transforms_train = trn.Compose([trn.RandomHorizontalFlip(), trn.RandomCrop(32, padding=2),trn.ToTensor(), trn.Normalize(mean, std)])
cifar_transforms_test = trn.Compose([trn.ToTensor(), trn.Normalize(mean.tolist(), std.tolist())])
minst_transform = trn.Compose([trn.ToTensor()])
transforms_dic = {
    "mnist":minst_transform,
    "cifar10_train":cifar_transforms_train,
    "cifar10_test":cifar_transforms_test,
}



def mnist_preprocessing(x_test):
    temp = np.copy(x_test)
    temp = temp.reshape(temp.shape[0], 1, 28, 28)
    temp = temp.astype('float32')
    temp /= 255
    return temp

def cifar_preprocessing(x_test): 
    temp = np.copy(x_test)
    temp = temp.astype('float32')
    mean = [125.307, 122.95, 113.865]
    std = [62.9932, 62.0887, 66.7048]
    for i in range(3):
        temp[:, :, :, i] = (temp[:, :, :, i] - mean[i]) / std[i]
    temp = temp.transpose(0,3,1,2)
    return temp

metrics_para = {
    'kmnc': 1000,
    'bknc': 10,
    'tknc': 10,
    'nbc': 10,
    'nc': 0.75,
    'snac': 10
}

preprocess_dic = {
    'lenet5': mnist_preprocessing,
    'lenet5_oe': mnist_preprocessing,
    'resnet18': cifar_preprocessing,
    'resnet18_oe': cifar_preprocessing,
    'densenet': cifar_preprocessing,
    'densenet_oe': cifar_preprocessing,
}

shape_dic = {
    'lenet5': (1, 28, 28),
    'lenet5_oe': (1, 28, 28),
    'resnet18': (3,32,32),
    'resnet18_oe':  (3,32,32),
    'densenet': (3,32,32),
    'densenet_oe':  (3,32,32),
}

model_profile_path = {
    'lenet5': "./profiling/lenet5_0_50000.pickle",
    'resnet18':"./profiling/resnet18_0_50000.pickle",
    'densenet':"./profiling/densenet_0_50000.pickle"
}

weight_path_dic = {
    "wrn":"/home/lingjun/ood_2020/data/pretrained/cifar10/CIFAR_pytorch(wrn)/wrn_baseline_epoch_84_0.9227.pt",
    "wrn_oe":"/home/lingjun/ood_2020/data/pretrained/cifar10/CIFAR_pytorch(wrn)/wrn_oe_epoch_70_0.9211.pt",
    "wrn_dropout":"/home/lingjun/ood_2020/data/pretrained/cifar10/CIFAR_pytorch(wrn)/wrn_baseline_epoch_84_0.9227.pt",
    
    
    "resnet18":"/home/lingjun/ood_2020/data/pretrained/cifar10/CIFAR_pytorch(resnet18)/resnet18_baseline_epoch_95_0.9144.pt",
    "resnet18_oe":"/home/lingjun/ood_2020/data/pretrained/cifar10/CIFAR_pytorch(resnet18)/resnet18_oe_epoch_95_0.8989.pt",
    
    
    "densenet":"/home/lingjun/ood_2020/data/pretrained/cifar10/CIFAR_pytorch(densenet)/densenet_baseline_epoch_185_0.9445.pt",
    "densenet_oe":"/home/lingjun/ood_2020/data/pretrained/cifar10/CIFAR_pytorch(densenet)/densenet_oe_epoch_156_0.9401.pt",
    
    
    "VGG13":"/home/lingjun/ood_2020/data/pretrained/cifar10/CIFAR_pytorch(VGG13)/VGG13_baseline_epoch_198_0.9397.pt",
    "VGG13_oe":"/home/lingjun/ood_2020/data/pretrained/cifar10/CIFAR_pytorch(VGG13)/VGG13_oe_epoch_183_0.9342.pt",
    
    "lenet5":"/home/lingjun/ood_2020/data/pretrained/mnist/MNIST_pytorch(lenet5)/lenet5_baseline_epoch_27_0.9898.pt",
    "lenet5_oe":"/home/lingjun/ood_2020/data/pretrained/mnist/MNIST_pytorch(lenet5)/lenet5_oe_epoch_47_0.9899.pt",
    "lenet5_dropout":"/home/lingjun/ood_2020/data/pretrained/mnist/MNIST_pytorch(lenet5)/lenet5_baseline_epoch_27_0.9898.pt",
    
    
    "lenet4":"/home/lingjun/ood_2020/data/pretrained/MNIST_pytorch(lenet4)/lenet4_baseline_epoch_33_0.9917.pt",
    "lenet4_oe":"/home/lingjun/ood_2020/data/pretrained/MNIST_pytorch(lenet4)/lenet4_oe_epoch_25_0.9912.pt",
    "lenet4_dropout":"/home/lingjun/ood_2020/data/pretrained/MNIST_pytorch(lenet4)/lenet4_baseline_epoch_33_0.9917.pt",
    
    "lenet1":"/home/lingjun/ood_2020/data/pretrained/MNIST_pytorch(lenet1)/lenet1_baseline_epoch_46_0.9898.pt",
    "lenet1_oe":"/home/lingjun/ood_2020/data/pretrained/MNIST_pytorch(lenet1)/lenet1_oe_epoch_45_0.9878.pt",
    "lenet1_dropout":"/home/lingjun/ood_2020/data/pretrained/MNIST_pytorch(lenet1)/lenet1_baseline_epoch_46_0.9898.pt",


}

model_dic = {
    "wrn": WideResNet(depth=28, num_classes=10, widen_factor=10),
    "wrn_dropout": WideResNet(depth=28, num_classes=10, widen_factor=10),
    "wrn_oe": WideResNet(depth=28, num_classes=10, widen_factor=10),

    'resnet18':ResNet18(num_c=10),
    'resnet18_oe':ResNet18(num_c=10),
    
    
    "densenet":densenet_cifar(),#(depth=40, num_classes=10),
    "densenet_oe":densenet_cifar(),#(depth=40, num_classes=10),
    
    
    "VGG13":VGG("VGG13"),
    "VGG13_oe":VGG("VGG13"),

    "lenet5":Lenet5( num_classes=10),
    "lenet5_dropout":Lenet5( num_classes=10,dropRate=0.5),
    "lenet5_oe":Lenet5( num_classes=10),

    

    "lenet4":Lenet4( num_classes=10),
    "lenet4_dropout":Lenet4( num_classes=10,dropRate=0.5),
    "lenet4_oe":Lenet4( num_classes=10),
    
    "lenet1":Lenet1( num_classes=10),
    "lenet1_dropout":Lenet1( num_classes=10,dropRate=0.5),
    "lenet1_oe":Lenet1( num_classes=10),
    
    "mnist_dae":DenoisingAutoEncoder(),
    
}

data_dic = {
    "mnist":'/home/lingjun/1026/OOD/public_dataset/mnist',
    "cifar10":'/home/lingjun/1026/OOD/public_dataset/cifar10',
}





