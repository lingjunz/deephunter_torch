import os
import torch
import argparse
import numpy as np
os.environ["CUDA_VISIBLE_DEVICES"] = "1"
import sys
sys.path.append("../")
from .configs import *
from .ood_loader import OODImages
from cal_functions.cal_acc import cal_accuracy


def initial_argparse():
    parser = argparse.ArgumentParser(description='coverage guided fuzzing for DNN')
    parser.add_argument('-class_num', help='class num',type=int, default=10)
    parser.add_argument('-data_type', help='data type', choices=['mnist','cifar10'],default="mnist")
    parser.add_argument('-i', help='input seed directory')
    parser.add_argument('-o', help='output directory')
    parser.add_argument('-oe_mode', help="whether the model is training with outlier exposure", choices=[0,1], default=0)
    parser.add_argument('-model', help="target model to fuzz", choices=['densenet', 'resnet18', 'mobilenet', 'vgg19',
                                                                        'resnet50', 'lenet1', 'lenet4', 'lenet5'],default='lenet5')
    parser.add_argument('-guided_mode', help="guided mode(0: only nc, 1: only ood, 2: ood + nc)",type=int,choices=[0,1,2], default=0)
    parser.add_argument('-ood_params', help="k-sections for ood", type=int, default=-1)
    parser.add_argument('-criteria', help="set the criteria to guide the fuzzing",
                        choices=['nc', 'kmnc', 'nbc', 'snac', 'bknc', 'tknc', 'ood'], default='kmnc')

    parser.add_argument('-comb_cri', help="set the criteria to guide the fuzzing",
                        choices=['nc', 'kmnc'], default='kmnc')         

    parser.add_argument('-metric_para', help="set the parameter for different metrics", type=float, default=-1)
    parser.add_argument('-batch_num', help="the number of mutants generated for each seed", type=int, default=20)
    parser.add_argument('-max_iteration', help="maximum number of fuzz iterations", type=int, default=1000)

    parser.add_argument('-quantize_test', help="fuzzer for quantization", default=0, type=int)
    # parser.add_argument('-ann_threshold', help="Distance below which we consider something new coverage.", type=float,
    #                     default=1.0)
    parser.add_argument('-quan_model_dir', help="directory including the quantized models for testing",default="")
    parser.add_argument('-random', help="whether to adopt random testing strategy", type=int, default=0)
    parser.add_argument('-select', help="test selection strategy",
                        choices=['uniform', 'tensorfuzz', 'deeptest', 'prob'], default='prob')
    args = parser.parse_args()
    return args


concat = lambda x: np.concatenate(x,axis=0)
to_np = lambda x: x.data.cpu().numpy()

def load_torch_data(data_type,net,transform_test,from_train = True,sample_num=0):
    from keras.datasets import cifar10,mnist
    if data_type == "mnist":
        (x_train,y_train),(x_test,y_test) = mnist.load_data()
        img_size = (28,28)
        batch_size = 256
    elif data_type == "cifar10":
        (x_train,y_train),(x_test,y_test) = cifar10.load_data()
        img_size = (32,32)
        batch_size = 256
    else:
        assert False, "undefined data type!"    
    x = x_train if from_train else x_test
    y = y_train if from_train else y_test
    
    dataloader = make_dataloader(x,y,
                                img_size=img_size, 
                                batch_size=batch_size, 
                                transform_test=transform_test,
                                shuffle=False)
    _,preds,_ = cal_accuracy(net,dataloader)
    correct_indexes = np.where(preds==y)[0]
    x,y = x[correct_indexes],y[correct_indexes]
    if sample_num <= 0:
        return x,y
    else:
        chosen_indexes = []
        for i in range(10):
            cur_indexes = np.random.choice(np.where(y==i)[0],sample_num,replace=False)
            chosen_indexes += list(cur_indexes)
        chosen_indexes = np.array(chosen_indexes)
        np.random.shuffle(chosen_indexes)
        return x[chosen_indexes],y[chosen_indexes]

def make_dataloader(samples,labels,img_size=(32,32), batch_size=256, transform_test=None,shuffle=False):
    ood_dataset = OODImages(samples,labels = labels,img_size = img_size, transform=transform_test)
    ood_loader = torch.utils.data.DataLoader(ood_dataset,batch_size=batch_size, shuffle=shuffle, num_workers=4, pin_memory=True)
    return ood_loader

def load_net(model_name,oe_detector=False,device=torch.device('cuda:0')):

    net = model_dic[model_name]
    weight_path = weight_path_dic[model_name]
    net.to(device)#cuda()
    net.load_state_dict(torch.load(weight_path))
    print("Detector:{}\nLoad {} model successfully!\nweight_path:{}".format(oe_detector,model_name,weight_path))
    # if torch.cuda.device_count() > 1:
    #     print("Using multiple GPUs")
    #     net = nn.DataParallel(net)
    net.eval()
    if oe_detector:
        ood_max,ood_min = range_dic[model_name]
        use_xent = True
        return net,use_xent,ood_max,ood_min
    else:
        return net

# def get_output(net,test_loader,layer_index = 3):
#     net.eval()
#     output = []
#     penultimate = []
#     with torch.no_grad():
#         for data,target in test_loader:
#             target = target.long()
#             data,target = data.cuda(),target.cuda()
#             # forward
#             _output,_penultimate = net.feature_list(data)
#             output.append(to_np(_output))
#             penultimate.append(to_np(_penultimate[layer_index]))
#     return concat(output), concat(penultimate)

# def make_mixed_dataloader(id_samples,ood_samples, img_size=(32,32), batch_size=256, transform_test=None):
#     '''
#         * make a mixed dataloader from benign ID and OOD samples
#         * label: ood->1,id->0
#         * original classifier: resnet18
#         * OOD metrics(**based on original DNN**): baseline, ODIN, mahalanobis
#         * OOD metrics(**based on additional detector**): OE, likelihood
#     '''
#     num = len(ood_samples)+len(id_samples)
#     ood_labels = np.ones(len(ood_samples))
#     id_labels = np.zeros(len(id_samples))
#     all_samples = np.concatenate((ood_samples,id_samples),axis=0)
#     all_labels = np.concatenate((ood_labels,id_labels),axis=0)
    
#     indexes = np.arange(num)
#     np.random.shuffle(indexes)
    
#     all_samples,all_labels = all_samples[indexes],all_labels[indexes]   

#     mix_dataloader = make_dataloader(all_samples,all_labels,img_size,batch_size,transform_test)
#     return mix_dataloader,all_labels


